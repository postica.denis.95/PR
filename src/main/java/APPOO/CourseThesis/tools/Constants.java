package APPOO.CourseThesis.tools;

/**
 * Created by Dennis on 23-May-17.
 **/
public final class Constants {
    public final class Server {
        public final static int PORT = 12340;
        //public final static long MAX_USERS = 2;

        public final static String PM = "[PM FROM]";
    }
    public final class Client {

        public final static String LIST_COMMAND = "Online";
        public final static String HELP_COMMAND = "Help";

        public final static String NEW_USER = "n";
        public final static String MESSAGE_COMMAND = "Send";
        //public final static String GROUP_MESSAGE_COMMAND = "gm";
        public final static String PRIVATE_MESSAGE_COMMAND = "PM";
        public final static String EXIT_COMMAND = "Exit";

        public final static String SEPARATOR = ";";

        public final static String HELP_TEXT = "\nPress '" + LIST_COMMAND + "' button to get the list of online users.\n" +
                "Press '" + HELP_COMMAND + "' button to display help message.\n" +
                "Press '" + EXIT_COMMAND + "' button to exit.\n" +
                "Press '" + PRIVATE_MESSAGE_COMMAND + "' button to send a private message to an online user.\n" +
                "Press '" + MESSAGE_COMMAND + "' button to send a message to all online users.\n" +
                "Have fun!\n";
    }
}

