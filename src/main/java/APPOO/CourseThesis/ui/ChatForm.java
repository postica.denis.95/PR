package APPOO.CourseThesis.ui;

import APPOO.CourseThesis.client.Client;
import APPOO.CourseThesis.tools.Constants;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;

/**
 * Created by Dennis on 18-Jun-17.
 **/
public class ChatForm {

    public static JTextArea chat;

    ChatForm(JFrame frame, Component loginPanel, Client client) {

        loginPanel.setVisible(false);
        frame.setPreferredSize(new Dimension(400, 600));
        frame.pack();

        frame.setLocationRelativeTo(null);
        frame.pack();
        frame.setTitle("IChat");

        JPanel chatPanel = new JPanel();
        chatPanel.setBorder ( new TitledBorder( new EtchedBorder(), client.getUser().getNickname() ) );
        chatPanel.setBackground(Color.black);

        chat = new JTextArea(25,30);
        chat.setEditable(false);
        chat.setBackground(Color.white);
        chat.setForeground(Color.black);

        final JTextArea messageBox = new JTextArea(5,31);
        messageBox.setBackground(Color.white);
        messageBox.setForeground(Color.black);

        JScrollPane chatScroll = new JScrollPane(chat);
        chatScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        JScrollPane messageScroll = new JScrollPane(messageBox);
        chatScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);


        JButton send = new JButton("Send");
        JButton exit = new JButton("Exit");
        JButton pm = new JButton("PM");
        JButton online = new JButton("Online");
        JButton help = new JButton("Help");

        send.addActionListener(e ->
                {

                    if (!messageBox.getText().equals("")) {
                        chat.append(client.getUser().getNickname() + ": " + messageBox.getText());
                        chat.append("\n");
                        client.sendMessage(messageBox.getText());
                        messageBox.setText(null);
                    }
                }
        );

        exit.addActionListener(e ->
                {

                    int dialogButton = JOptionPane.YES_NO_OPTION;
                    int dialogResult = JOptionPane.showConfirmDialog (frame, "Are you sure you want to exit?","Warning",dialogButton);
                    if(dialogResult == JOptionPane.YES_OPTION){
                        client.sendExitMessage();
                    }
                }
        );

        help.addActionListener(e ->
                JOptionPane.showMessageDialog(frame, Constants.Client.HELP_TEXT, "Information", JOptionPane
                        .INFORMATION_MESSAGE)
        );

        online.addActionListener(e ->
                client.outputOnlineUsers()
        );

        pm.addActionListener(e ->{

                String nickname = JOptionPane.showInputDialog(frame, "Introduce the user's username.");

                if (nickname.equals(client.getUser().getNickname())) {
                    LoginForm.infoBox(frame, "You can not send a pm to yourself! Try again.", "Error!", JOptionPane.ERROR_MESSAGE);
                }
                else if (client.validatePMUser(nickname)) {

                    String message = JOptionPane.showInputDialog(frame, "Introduce the message.");

                    chat.append("[PM TO] " + nickname + ": " + message);
                    client.sendPMessage(nickname, message);
                }
                else {
                    LoginForm.infoBox(frame, "No such user with username: " + nickname + "! Try again.", "Error!", JOptionPane.ERROR_MESSAGE);
                }
            }
        );

        chatPanel.add(chatScroll);
        chatPanel.add(messageScroll);
        chatPanel.add(exit);
        chatPanel.add(help);
        chatPanel.add(online);
        chatPanel.add(pm);
        chatPanel.add(send);

        frame.add(chatPanel);

        LoginForm.infoBox(frame, "Welcome to IChat!", "Welcome!", JOptionPane.INFORMATION_MESSAGE);
    }
}
