package APPOO.CourseThesis.ui;

import APPOO.CourseThesis.client.Client;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Dennis on 18-Jun-17.
 **/
public final class LoginForm {

    public static JPanel loginPanel;
    private static int cnt = 0;
    private Client client;

    LoginForm(JFrame frame) {

        frame.setTitle("Login");

        loginPanel = new JPanel();

        loginPanel.setFocusable(true);
        loginPanel.requestFocusInWindow();

        loginPanel.setBackground(Color.black);

        JTextField host = new JTextField("localhost",15);
        JTextField nickname = new JTextField(15);
        JButton login = new JButton("Login");

        host.setBounds(10,50,30,30);
        host.setBackground(Color.white);
        host.setForeground(Color.black);
        host.setHorizontalAlignment(JTextField.CENTER);
        host.setToolTipText("Name of host computer");

        nickname.setBounds(1, 100, 30, 30);
        nickname.setBackground(Color.white);
        nickname.setForeground(Color.black);
        nickname.setHorizontalAlignment(JTextField.CENTER);
        nickname.setToolTipText("Your nickname");

        login.setBounds(90,10,20,20);
        login.setBackground(Color.black);
        login.setForeground(Color.white);

        frame.getRootPane().setDefaultButton(login);

        login.addActionListener(e ->
            {

                if (!host.getText().equals("") && !nickname.getText().equals("")) {
                    if (Client.isValid(nickname.getText()) && cnt == 0) {
                        client = new Client(host.getText(), nickname.getText());
                        client.connectClient();
                        cnt ++;

                        if (!client.validateUser(client.getUser().getNickname())){
                            infoBox(frame,"This nickname is already used!", "Error : Used nickname", JOptionPane.ERROR_MESSAGE);
                            return;
                        }

                        new ChatForm(frame, loginPanel, client);
                    }
                    else if (Client.isValid(nickname.getText()) && cnt > 0) {
                        client.getUser().setNickname(nickname.getText());
                        if (!client.validateUser(client.getUser().getNickname())){
                            infoBox(frame,"This nickname is already used!", "Error : Used nickname", JOptionPane.ERROR_MESSAGE);
                            return;
                        }
                        new ChatForm(frame, loginPanel, client);
                    }
                    else {
                        infoBox(frame, "Invalid nickname. A nickname can only contain letters and numbers. Try again.",
                                "Error : Invalid nickname", JOptionPane.ERROR_MESSAGE);
                    }
                }
                else {
                    infoBox(frame, "Empty fields are not allowed","Error : Invalid data", JOptionPane.ERROR_MESSAGE);
                }
            }
        );

        loginPanel.add(host);
        loginPanel.add(nickname);
        loginPanel.add(login);

        frame.add(loginPanel);
    }

    public static void infoBox(Component frame, String infoMessage, String titleBar, int option)
    {
        JOptionPane.showMessageDialog(frame, infoMessage, titleBar, option);
    }
}
