package APPOO.CourseThesis.server;

import APPOO.CourseThesis.tools.Constants;

/**
 * Created by Dennis on 23-May-17.
 **/
public class ServerMain {

    public static void main(String argv[]) throws Exception {

        new Server(Constants.Server.PORT);
    }
}
