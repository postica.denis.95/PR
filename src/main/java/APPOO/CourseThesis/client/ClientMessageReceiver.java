package APPOO.CourseThesis.client;

import APPOO.CourseThesis.tools.Constants;
import APPOO.CourseThesis.ui.ChatForm;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * Created by Dennis on 25-May-17.
 **/
public class ClientMessageReceiver extends Thread {

    private Stack<String> messagesStack;
    private BufferedReader in;

    public ClientMessageReceiver(BufferedReader in) {
        this.in = in;
        messagesStack = new Stack<>();
    }

    public void run() {

        try {
            String fromClient;

            while (true) {
                fromClient = in.readLine();
                List<String> serverPackage;

                try {
                    serverPackage = Arrays.asList(fromClient.split(Constants.Client.SEPARATOR));
                }
                catch (NullPointerException e) {
                    break;
                }

                messagesStack.push(fromClient);

                if (serverPackage.get(0).equals(Constants.Client.MESSAGE_COMMAND)) {

                    ChatForm.chat.append(serverPackage.get(1));
                    ChatForm.chat.append("\n");
                    messagesStack.pop();
                }
            }
        } catch (IOException e) {

            this.interrupt();
        }
    }

    public String getMessage() {
        return messagesStack.pop();
    }

    public boolean noMessages() {
        return messagesStack.empty();
    }
}
